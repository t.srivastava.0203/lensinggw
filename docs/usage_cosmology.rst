| By default, a flat cosmology with :math:`H_0=69.7, \Omega_0=0.306, T_{cmb0}=2.725` is considered to convert the 
  lens and source redshifts :math:`z_L` and :math:`z_S` to angular diameter distances. However, users can indicate a different one
  by specifying an instance of the astropy cosmology class to *TimeDelay*, *lensed_gw* and to *lensed_snr* through the *cosmo* keyword argument

.. code-block:: python
        
        # time delays, magnifications, Morse indices
        TimeDelay(Img_ra, Img_dec,
                     .
                     .
                    cosmo = asropy_cosmo_instance)    
  
.. code-block:: python
 
        # compute the lensed waveform polarizations, strains in the requested detectors and their frequencies
        waveform_model.lensed_gw(Img_ra, Img_dec,
                                  .
                                  .
                                 cosmo = asropy_cosmo_instance)

.. code-block:: python

        # and their signal-to-noise-ratios
        lensed_SNR_dict = waveform_model.lensed_snr(Img_ra, Img_dec,
                                                     .
                                                     .
                                                    cosmo = asropy_cosmo_instance)

where omitted lines are unchanged.
