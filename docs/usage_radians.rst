Radians
=======
* `Example 1 - Solve the lens model (radians) <https://gitlab.com/gpagano/lensinggw/-/blob/master/lensinggw/examples/binary_point_mass.py>`__

We first  define the lens configuration: we use *radians*, which is ``lensingGW``'s default. 
As an illustration, we consider two point lenses with Einstein radii :math:`\theta_{E_1}` and :math:`\theta_{E_2}`, whose parameters we have defined in *kwargs_lens_list*

.. code-block:: python
   
        # lens model               
        lens_model_list  = ['POINT_MASS', 'POINT_MASS'] 
        kwargs_lens_list = [{'center_x': 6.9e-11,'center_y': 0.0, 'theta_E': thetaE1} , {'center_x': -6.9e-11,'center_y': 0.0, 'theta_E': thetaE2}]   

We then specify the solver settings

.. code-block:: python

        # solver setup
        solver_settings = {'SearchWindowMacro': 4*thetaE1,
                           'SearchWindow'     : 4*thetaE2}  

| The solver implements the *two-step-procedure* presented in `Pagano et al. 2020 <https://www.aanda.org/articles/aa/full_html/2020/11/aa38730-20/aa38730-20.html>`_. The minimal setup to specify
  consists in the extension of the search region for the macromodel, *SearchWindowMacro* and for the complete model, *SearchWindow*.
| Finally, we import the solver routine and solve for the image positions
                      
.. code-block:: python

        from lensinggw.solver.images import microimages
        
        # solve for the image positions with the two-step procedure
        Img_ra, Img_dec, MacroImg_ra, MacroImg_dec, pixel_width  = microimages(source_pos_x    = 1.39e-11,
                                                                               source_pos_y    = 1.20e-10,
                                                                               lens_model_list = lens_model_list,
                                                                               kwargs_lens     = kwargs_lens_list,
                                                                               **solver_settings)

| We obtain the right ascensions and the declinations of the images of the complete model, of the macromodel and the pixel
  width of the last iteration, in the same units as the input.
| The description of the each iteration step can be enabled through the *Verbose* option in the solver settings.

.. code-block:: python

        solver_settings.update({'Verbose' : True})
        
The verbosity is a useful diagnostic in non-converging cases, as it allows to identify the problematic part of the 
iteration.

**Image properties** 
 
Magnifications, time delays and Morse indices can be accessed through the related modules

.. code-block:: python

        from lensinggw.utils.utils import TimeDelay, magnifications, getMinMaxSaddle
        
        # time delays, magnifications, Morse indices
        tds = TimeDelay(Img_ra, Img_dec,
                        source_pos_x = 1.39e-11, source_pos_y = 1.20e-10,
                        zL = 0.5 , zS = 2.0,
                        lens_model_list, kwargs_lens_list)  
                        
        mus = magnifications(Img_ra, Img_dec, lens_model_list, kwargs_lens_list)
        ns  = getMinMaxSaddle(Img_ra, Img_dec, lens_model_list, kwargs_lens_list) 
  
By default, the solver considers the first profile in *lens_model_list* as macromodel and applies the two-step procedure to all
the macroimages. It sets the same precision requirement on both types of images, as well as the same number of pixels and the same grid sizes
for the iterations. These items can be customized through the solver settings. A detailed description of the available options
can be found in the `solver technical notes <https://gpagano.gitlab.io/lensinggw/notes.html#the-solver>`_.