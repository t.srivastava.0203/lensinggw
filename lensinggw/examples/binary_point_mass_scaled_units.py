# This script illustrates how to use lensingGW to solve a binary point mass lens model, assuming scaled units

import numpy as np

# coordinates in scaled units [x (radians) /thetaE_tot]
y0,y1 = 0.1,0.5*np.sqrt(3)   
l0,l1 = 0.5,0  

# redshifts                                                                                                                  
zS = 2.0 
zL = 0.5  

# masses 
mL1  = 100                                                                   
mL2  = 100  
mtot = mL1+mL2  

# compute the scale factor and the individual Einstein radii
from lensinggw.utils.utils import param_processing                                                                                                                        
thetaE  = param_processing(zL, zS, mtot)  
thetaE1 = param_processing(zL, zS, mL1)                                                                                                                              
thetaE2 = param_processing(zL, zS, mL2) 

# lens model, give all input in scaled units
lens_model_list            = ['POINT_MASS', 'POINT_MASS'] 
kwargs_point_mass_1_scaled = {'center_x': l0,'center_y': l1, 'theta_E': thetaE1/thetaE}  # convert thetaE1 from radians to scaled units
kwargs_point_mass_2_scaled = {'center_x': -l0,'center_y': l1, 'theta_E': thetaE2/thetaE} # convert thetaE2 from radians to scaled units
kwargs_lens_list_scaled    = [kwargs_point_mass_1_scaled, kwargs_point_mass_2_scaled]  

# indicate the first lens as macromodel and solve with the two-step procedure, give all input in scaled units
from lensinggw.solver.images import microimages

solver_kwargs_scaled = {'Scaled'           : True,   # indicate that the input is in scaled units 
                        'ScaleFactor'      : thetaE, # and the scale factor  
                        'SearchWindowMacro': 4*thetaE1/thetaE, # convert search windows from radians to scaled units
                        'SearchWindow'     : 4*thetaE2/thetaE} # convert search windows from radians to scaled units  

Img_ra_scaled, Img_dec_scaled, MacroImg_ra_scaled, MacroImg_dec_scaled, pixel_width  = microimages(source_pos_x    = y0,
                                                                                                   source_pos_y    = y1,
                                                                                                   lens_model_list = lens_model_list,
                                                                                                   kwargs_lens     = kwargs_lens_list_scaled,
                                                                                                   **solver_kwargs_scaled)                                                            
                                                                       
# time delays, magnifications, Morse indices and amplification factor
from lensinggw.utils.utils import TimeDelay, magnifications, getMinMaxSaddle
from lensinggw.amplification_factor.amplification_factor import geometricalOpticsMagnification

tds = TimeDelay(Img_ra_scaled, Img_dec_scaled,
                y0, y1,
                zL, zS,
                lens_model_list, kwargs_lens_list_scaled,
                scaled       = solver_kwargs_scaled['Scaled'],      # indicate that the input is in scaled units 
                scale_factor = solver_kwargs_scaled['ScaleFactor']) # and the scale factor              
mus = magnifications(Img_ra_scaled, Img_dec_scaled, lens_model_list, kwargs_lens_list_scaled)
ns  = getMinMaxSaddle(Img_ra_scaled, Img_dec_scaled, lens_model_list, kwargs_lens_list_scaled) 
                
print('Time delays (seconds): ', tds)
print('magnifications: ',  mus)
print('Morse indices: ',ns)

dummy_frequencies = np.linspace(0,10,11)
F = geometricalOpticsMagnification(dummy_frequencies,
                                   Img_ra_scaled,Img_dec_scaled,
                                   y0,y1,
                                   zL,zS,
                                   lens_model_list,
                                   kwargs_lens_list_scaled,
                                   scaled       = solver_kwargs_scaled['Scaled'],      # indicate that the input is in scaled units 
                                   scale_factor = solver_kwargs_scaled['ScaleFactor']) # and the scale factor   

print('Geometrical optics amplification factor:', F)